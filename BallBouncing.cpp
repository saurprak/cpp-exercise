using namespace std;
#include <iostream>

class Ball
{

public:
    int radius;
    int color;
    Ball(int radius, int color)
    {
        Ball::radius = radius;
        Ball::color = color;
    }
};
class Boundary
{
private:
public:
    int height;
    int width;
    Boundary(int height, int width)
    {
        Boundary::height = height;
        Boundary::width = width;
    }
};

class Location
{

private:
public:
    int x;
    int y;
    int vx;
    int vy;

    Location();
    Location(int x, int y, int vx, int vy)
    {
        Location::x = x;
        Location::y = y;
        Location::vx = vx;
        Location::vy = vy;
    }
};
class BallBouncing
{
private:
public:
    void draw(Location start, Ball ball, Boundary boundry);
    Location moveOrBounce(Location location, Ball ball, Boundary boundry);
};

void BallBouncing::draw(Location start, Ball ball, Boundary boundary)
{
    /**
     * Draw logic from graphic interface.
     **/
}

Location BallBouncing::moveOrBounce(Location start, Ball ball, Boundary boundary)
{
    int vx = start.vx;
    int vy = start.vy;
    int x = start.x + vx;
    int y = start.y + vy;
    if (x - ball.radius <= 0)
    {
        x = 0 + ball.radius;
        vx *= -1;
        std::cout << "bounce left";
        std::cout << endl;
    }

    else if (x + ball.radius >= boundary.width)
    {
        x = boundary.width - ball.radius;
        vx *= -1;
        std::cout << "bounce right";
        std::cout << endl;
    }

    else if (y - ball.radius <= 0)
    {
        y = 0 + ball.radius;
        vy *= -1;
        std::cout << "bounce top";
        std::cout << endl;
    }

    else if (y + ball.radius >= boundary.height)
    {
        y = boundary.height - ball.radius;
        vy *= -1;
        std::cout << "bounce bottom";
        std::cout << endl;
    }
    return Location(x, y, vx, vy);
}

int main()
{
    Location start(0, 0, 3, 3);
    Ball ball(4, 1);
    Boundary boundry(100, 200);
    BallBouncing ballBouncing;
    ballBouncing.draw(start, ball, boundry);
    while (true)
    {
        start = ballBouncing.moveOrBounce(start, ball, boundry);
    }
}